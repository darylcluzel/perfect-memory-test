import { stringify } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { MediaType, Movie } from '../movie/movie.model';

@Injectable()
export class StorageService {
  // public seenMedia = new BehaviorSubject(this.getStorage('seen'));
  private storageData = new Subject<Movie[]>();
  private updating = new BehaviorSubject<boolean>(false);

  private storage: Storage = localStorage;

  constructor() {}

  /**
   * @param key The localStorage key, enabling to make multiple type of localStorage. eg media 'seen'/'to-see'
   * @returns Undefined if there's no localStorage is empty or the parsed localStorage
   */
  getStorage(key: string): Array<Movie> | undefined {
    let storedItem = this.storage.getItem(key) as string;
    if (storedItem === null) {
      return;
    }
    return JSON.parse(storedItem);
  }

  /**
   * Add an entry to the localStorage
   * If localStorage is undefined, set the Array
   * Check if media to add is in storage, if not the media is added
   * @param media The media to add
   * @param storageType The localStorage key, enabling to make multiple type of localStorage. eg media 'seen'/'to-see'
   * @returns true : the media is addded
   */
  addStorageData(media: Movie, storageType: string): boolean | string {
    let store = this.getStorage(storageType);
    const len = store?.length;
    let isAdded: boolean | string;

    const isMediaAlreadyStored = this.isMediaAlreadyStored(media, storageType);
    // Check if store is already in
    if (!isMediaAlreadyStored) {
      // if media doesn't exist in storage add it, otherwise it
      if (store === undefined) {
        store = new Array(media) as Array<Movie>;
      } else {
        store.push(media);
      }
      isAdded = true;
      this.storage.setItem(storageType, JSON.stringify(store));
    } else {
      if (typeof isMediaAlreadyStored === 'string') {
        isAdded = isMediaAlreadyStored;
      } else {
        isAdded = false;
      }
    }
    // }
    return isAdded;
  }

  /**
   * Extract localStorage & remove array item, then re-set localStorage
   * @param media
   * @param storageType
   * @returns
   */
  removeStorageData(media: Movie, storageType: string): boolean | string {
    const store = this.getStorage(storageType);
    if (store !== undefined) {
      store.find((value: Movie, index: number) => {
        if (value !== undefined) {
          if (value.id === media.id) {
            store?.splice(index, 1);
            this.storage.setItem(storageType, JSON.stringify(store));
          }
        }
      });
      return true;
    } else {
      return "This media isn't in your" + storageType + 'list';
    }
  }

  /**
   * Transfer media from one LocalStorage to the other one
   * @param movie
   * @param storageType
   * @returns
   */
  changeMediaList(media: Movie, storageType: string): boolean {
    const storeType = storageType === 'to-see' ? 'seen' : 'to-see';
    this.removeStorageData(media, storageType);
    this.addStorageData(media, storeType);
    return true;
  }

  /**
   * Check if data is in any LocalStorage store
   * @param media the media to add
   * @param storageType where to store data
   * @returns false if media isn't in the store
   */
  isMediaAlreadyStored(media: Movie, storageType: string): boolean | string {
    const store = this.getStorage(storageType);
    const otherStoreName = storageType === 'to-see' ? 'seen' : 'to-see';
    const otherStore = this.getStorage(otherStoreName);
    // find a media by id
    const isMediaInStore = store?.find((value: Movie) => {
      return value.id === media.id;
    });
    // Check if media is in the other store
    const isMediaInOtherStore = otherStore?.find((value: Movie) => {
      return value.id === media.id;
    });
    // If storedMedia has been found return true otherwise return that the media is in the other store
    return !isMediaInStore
      ? isMediaInOtherStore
        ? ' is already in your ' + otherStoreName.replace('-', ' ')
        : false
      : true;
  }

  /**
   * @WIP: LocalStorage subscribe
   */
  isUpdating() {
    return this.updating.asObservable();
  }

  /**
   * @WIP: LocalStorage subscribe
   */
  getStorageData(): Observable<Movie[]> {
    return this.storageData;
  }
}
