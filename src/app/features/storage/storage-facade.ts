import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Movie } from '../movie/movie.model';
import { StorageService } from './storage.service';

@Injectable()
export class StorageFacade {
  constructor(private storageService: StorageService) {}

  isUpdating$(): Observable<boolean> {
    return this.storageService.isUpdating();
  }

  getMedia(key: string): Movie[] {
    // return this.storageService.seenMedia;
    return this.storageService.getStorage(key) as Movie[];
  }

  getDataSubscriber(): Observable<Movie[]> {
    return this.storageService.getStorageData();
  }

  addMedia(media: Movie, type: string): boolean | string {
    return this.storageService.addStorageData(media, type);
  }

  changeList(media: Movie, type: string): boolean {
    return this.storageService.changeMediaList(media, type);
  }

  removeMedia(media: Movie, type: string): boolean | string {
    return this.storageService.removeStorageData(media, type);
  }

  // loadCashflowCategories() {
  //   return this.cashflowCategoryApi.getCashflowCategories()
  //     .pipe(tap(categories => this.settingsState.setCashflowCategories(categories)));
  // }

  // optimistic update
  // 1. update UI state
  // 2. call API
  // addCashflowCategory(category: CashflowCategory) {
  //   this.cashflowCategoryApi.createCashflowCategory(category)
  //     .subscribe(
  //       (addedCategoryWithId: CashflowCategory) => {
  //         // success callback - we have id generated by the server, let's update the state
  //         this.settingsState.updateCashflowCategoryId(category, addedCategoryWithId)
  //       },
  //       (error: any) => {
  //         // error callback - we need to rollback the state change
  //         this.settingsState.removeCashflowCategory(category);
  //         console.log(error);
  //       }
  //     );
  // }

  // pessimistic update
  // 1. call API
  // // 2. update UI state
  // updateCashflowCategory(category: CashflowCategory) {
  //   this.settingsState.setUpdating(true);
  //   this.cashflowCategoryApi.updateCashflowCategory(category)
  //     .subscribe(
  //       () => this.settingsState.updateCashflowCategory(category),
  //       (error) => console.log(error),
  //       () => this.settingsState.setUpdating(false)
  //     );
  // }
}
