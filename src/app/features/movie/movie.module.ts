import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDividerModule } from '@angular/material/divider';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatToolbarModule } from '@angular/material/toolbar';
import { YouTubePlayerModule } from '@angular/youtube-player';

import { StoreModule } from '@ngrx/store';
import { movieReducer } from './+state/movie.reducer';

import { SharedModule } from '../../shared/shared.module';

import { ItemComponent } from './item/item.component';
import { ListComponent } from './list/list.component';
import { MovieComponent } from './movie.component';
import { MovieRoutingModule } from './movie-routing.module';
import { PlayerComponent } from './item/player/player.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MovieRoutingModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatDividerModule,
    MatListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    YouTubePlayerModule,
    MatChipsModule,
    MatBottomSheetModule,
    InfiniteScrollModule,
    MatRadioModule,
    StoreModule.forFeature('MovieFeature', {
      movies: movieReducer,
    }),
  ],
  declarations: [MovieComponent, ListComponent, ItemComponent, PlayerComponent],
})
export class MovieModule {}
