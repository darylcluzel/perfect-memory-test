import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as MediaActions from '../+state/movie.actions';
import MovieState from '../+state/movie.state';
import { Cast, Movie } from '../movie.model';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { BottomSheetData } from 'src/app/shared/bottom-sheet/bottom-sheet.component';
import { ConfigService } from '../../../core/config.service';
import { BottomSheetComponent } from '../../../shared/bottom-sheet/bottom-sheet.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from 'src/app/shared/snack-bar/snack-bar.component';
import { StorageFacade } from '../../storage/storage-facade';

@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit, OnDestroy {
  public media: Movie = {
    credits: { cast: [], crew: [] },
    videos: { results: [] },
  };

  // manipulated media data
  public cast: Cast[] = [];
  public director: Cast[] = [];

  public bottomSheetActions: BottomSheetData[] = [];
  private mediaType = '';

  constructor(
    private route: ActivatedRoute,
    // private _bottomSheet: MatBottomSheet,
    private _snackBar: MatSnackBar,
    private config: ConfigService,
    private storage: StorageFacade
  ) {
    this.media = this.route.snapshot.data['movie'];
    this.route.fragment.subscribe((observer: string) => {
      this.mediaType = observer;
    });
  }
  ngOnInit(): void {
    this.cast = this.media.credits?.cast.slice(0, 5) as Cast[];
    this.getBottomSheetData();
    this.getDirector();
  }

  /**
   * Get json data to configure templater
   */
  async getBottomSheetData(): Promise<void> {
    this.bottomSheetActions = await this.config.get('browse_bottom_sheet');
  }

  // @TODO : enable users to directly remove or change item of mediaList
  // require to check which storage is used by item
  openBottomSheet(listType: string): void {
    // this._bottomSheet.open(BottomSheetComponent, {
    //   data: this.bottomSheetActions,
    // });
    // this._bottomSheet._openedBottomSheetRef
    //   ?.afterDismissed()
    //   .subscribe((value) => {
    //     console.log(value);
    //   });
  }

  /**
   * Open snackBar to respond user action
   * @param data the data from GetBottomSheetData()
   */
  openSnackBar(data: BottomSheetData) {
    const success = this.storage[data?.options?.func as keyof StorageFacade](
      this.media as Movie & string,
      data?.options?.type as string
    );
    const cssClass =
      typeof success === 'string' || !success
        ? 'snack-bar-error'
        : 'snack-bar-valid';

    const payload = {
      options: {
        value: data.options,
        mediaTitle: this.media.title || this.media.name,
      },
      success,
    };
    console.log(payload);
    this._snackBar.openFromComponent(SnackBarComponent, {
      duration: 5000,
      data: payload,
      panelClass: cssClass,
    });
  }

  /**
   * set director depending of meddia type (props differ)
   */
  getDirector() {
    this.mediaType === 'tv'
      ? (this.director = this.media.created_by as Cast[])
      : (this.director = this.media.credits.crew.filter(
          (value: Cast) => value.department === 'Directing'
        ));
  }

  ngOnDestroy(): void {
    console.log('destroyed');
  }
}
