import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Movies } from '../../movie.model';
import { YouTubePlayer } from '@angular/youtube-player';

const API_KEY = 'AIzaSyAnLtZNN2UdAujUJtFl35yZDuon8yZUT4U';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent implements OnInit, AfterViewInit {
  @ViewChild('player_container') playerContainer: any;
  @Input() data: Movies[] | undefined;
  public video: Movies = {};
  private apiLoaded = false;

  constructor() {}

  ngOnInit() {
    // Only search for trailer movie
    // @TODO: create a multi player
    if (this.data !== undefined) {
      this.video = this.data.find((value: Movies) => {
        return value.type === 'Trailer';
      }) as Movies;
    }
    console.log(this.video);
  }

  ngAfterViewInit(): void {
    if (!this.apiLoaded) {
      // This code loads the IFrame Player API code asynchronously, according to the instructions at
      // https://developers.google.com/youtube/iframe_api_reference#Getting_Started
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';
      document.body.appendChild(tag);
      this.apiLoaded = true;
    }
  }
}
