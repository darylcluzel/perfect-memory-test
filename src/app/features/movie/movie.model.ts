// export interface Show {
export enum MediaType {
  TV = 'tv',
  Movie = 'movie',
}
// }

export interface Movies {
  id?: number;
  key?: string;
  name?: string;
  site?: string;
  size?: string;
  type?: string;
}

export interface Cast {
  id?: string;
  name?: string;
  department?: string;
}

// @TODO: Use Builder for each type of Media
// @TODO: Rename every item called "Movie" by "Medias"
export interface Movie {
  media_type?: MediaType;
  adult?: boolean;
  id?: number;
  created_by?: Cast[];
  credits: { cast: Cast[]; crew: Cast[] };
  name?: string;
  first_air_date?: string;
  backdrop_path?: string;
  genre_ids?: Array<number>;
  original_language?: string;
  original_title?: string;
  overview?: string;
  popularity?: string;
  poster_path?: string;
  release_date?: string;
  title?: string;
  video?: boolean | {};
  videos: { results: Movies[] };
  vote_average?: number;
  vote_count?: number;
  my_mark?: number | null;
}
