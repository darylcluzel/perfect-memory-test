import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Movie } from '../movie.model';

export interface response {
  page: number;
  results: Movie[] | Movie;
  total_pages?: number;
  total_results?: number;
}
// api.themoviedb.org/3/discover/movie?api_key=ce8a07fc64ce531a2e83f08a286747cb&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1
@Injectable({ providedIn: 'root' })
export class MovieService {
  readonly API = 'https://api.themoviedb.org/3/';
  readonly API_KEY = 'ce8a07fc64ce531a2e83f08a286747cb';

  constructor(private http: HttpClient) {}

  public getAll(type?: string): Observable<response> {
    return this.http.get<response>(
      this.API + 'trending/all/day?api_key=' + this.API_KEY,
      {}
    );
  }

  public getByQuery(query: string, page: number): Observable<response> {
    let URIquery = encodeURI(query);
    return this.http.get<response>(
      this.API +
        'search/multi?api_key=' +
        this.API_KEY +
        '&query=' +
        URIquery +
        '&page=' +
        page
    );
  }

  public getById(type: string, _id: string): Observable<Movie> {
    return this.http.get<Movie>(
      this.API +
        type +
        '/' +
        _id +
        '?api_key=' +
        this.API_KEY +
        '&append_to_response=videos,credits'
    );
  }
}
