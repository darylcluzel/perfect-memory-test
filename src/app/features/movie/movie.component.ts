import { Component, ComponentRef, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import MovieState from './+state/movie.state';
import * as MovieActions from './+state/movie.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { UntilDestroy } from '@ngneat/until-destroy';
import { BehaviorSubject } from 'rxjs';
import { filterDataByType } from './+state/movie.actions';

@UntilDestroy()
@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss'],
})
export class MovieComponent implements OnInit {
  public form: FormGroup = new FormGroup({});
  public isLoading = new BehaviorSubject<boolean>(false);
  public hasSearch = false;

  /**
   * Getters accessing FormGroup nodes
   */
  get search(): FormControl {
    return this.form.get('search') as FormControl;
  }
  get filters(): FormControl {
    return this.form.get('filters') as FormControl;
  }

  constructor(
    private store: Store<{ movies: MovieState }>,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.form = this.fb.group({
      search: this.fb.control(null),
      filters: this.fb.group({
        date: this.fb.group({
          startDate: this.fb.control(''),
          endDate: this.fb.control(''),
        }),
        type: this.fb.control(''),
      }),
    });
  }

  ngOnInit() {
    console.log('MovieComponent Inited');
    this.search.valueChanges.subscribe(() => {
      this.searchMedia();
    });
  }

  /**
   * Triggered on search input change
   * check if user search from an item => redirect
   * dispatch the search to the store
   */
  searchMedia() {
    const fragment = this.route.fragment.subscribe((value) => {
      return value;
    });
    if (fragment !== undefined) {
      this.router.navigate(['/media']);
    }
    const query = this.form.get('search')?.value;
    if (query) {
      this.hasSearch = true;
      this.store.dispatch(MovieActions.loadBySearch({ query, page: 1 }));
    }
  }

  /**
   * Reset default media view
   */
  clearSearch() {
    this.hasSearch = false;
    this.search.setValue(null);
    this.store.dispatch(MovieActions.loadDefault());
  }

  filterMedia() {
    console.log(this.filters);
  }
}
