import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MovieComponent } from './movie.component';
import { ListComponent } from './list/list.component';
import { ItemComponent } from './item/item.component';
import { MovieResolverService } from './resolver/movie-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: MovieComponent,
    children: [
      {
        path: '',
        component: ListComponent,
      },
      {
        path: ':id',
        component: ItemComponent,
        resolve: { movie: MovieResolverService },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

  exports: [RouterModule],
})
export class MovieRoutingModule {}
