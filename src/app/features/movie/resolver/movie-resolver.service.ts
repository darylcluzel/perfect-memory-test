import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { EMPTY, merge, Observable, of } from 'rxjs';
import { MovieService } from '../api/movie.service';
import { Movie } from '../movie.model';
import { Store } from '@ngrx/store';
import * as MovieActions from '../+state/movie.actions';
import { take, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class MovieResolverService implements Resolve<Movie> {
  private id = '';
  private type = '';

  constructor(private service: MovieService, private store: Store<Movie>) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    this.id = route.paramMap.get('id') as string;
    this.type = route.fragment;
    return this.service.getById(this.type, this.id).pipe(
      take(1),
      mergeMap((data) => {
        console.log(data);
        return !!data ? of(data) : EMPTY;
      })
    );
  }
}
