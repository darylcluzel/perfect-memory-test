/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MovieResolverService } from './movie-resolver.service';

describe('Service: MovieResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MovieResolverService]
    });
  });

  it('should ...', inject([MovieResolverService], (service: MovieResolverService) => {
    expect(service).toBeTruthy();
  }));
});
