import { Action, createReducer, on, State } from '@ngrx/store';
import { Movie } from '../movie.model';
import * as MovieActions from './movie.actions';
import MovieState, { initializeState } from './movie.state';

// Set initialState of states
// @TODO : init with localStorage my-media states
export const initialState = initializeState();

// Reducer henling all action related to medias
const _movieReducer = createReducer(
  initialState,
  on(MovieActions.loadDefault, (state) => state),
  on(MovieActions.loadBySearch, (state, props) => {
    return state;
  }),
  on(MovieActions.loadSuccess, (state: MovieState, { payload }) => {
    return { ...state, Movies: payload, MovieError: null };
  }),
  on(MovieActions.getById, (state) => state),
  on(MovieActions.getByIdSuccess, (state, { payload }) => {
    return { ...state, Movies: payload, MovieError: null };
  })
);

export function movieReducer(
  state: MovieState | undefined,
  action: Action
): MovieState {
  return _movieReducer(state, action);
}
