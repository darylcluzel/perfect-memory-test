import { createAction, props } from '@ngrx/store';
import { Movie } from '../movie.model';

export const loadDefault = createAction('[Movie Component] LoadMovieDefault');
export const loadBySearch = createAction(
  '[Movie Component] LoadMovieBySearch',
  props<{ page: number; query: string }>()
);

export const loadSuccess = createAction(
  '[Movie Component] LoadMovieSuccess',
  props<{ payload: Movie[] }>()
);
export const loadError = createAction(
  '[Movie Component] LoadError',
  props<Error>()
);
export const getById = createAction(
  '[Media Component] GetById',
  props<{ mediaType: string; _id: string }>()
);
export const getByIdSuccess = createAction(
  '[Media Component] GetByIdSuccess',
  props<{ payload: Movie }>()
);

export const getByIdError = createAction(
  '[Media Component] GetByIdError',
  props<{ error: Error }>()
);

export const searchMedia = createAction(
  '[Media Component] SearchMedia',
  props<{ query: string }>()
);

export const filterDataByDate = createAction(
  '[Media Component] FilterDataByDate',
  props<{ startDate?: string; enDate?: string }>()
);
export const filterDataByType = createAction(
  '[Media Component] FilterDataByType',
  props<{ filterType: string }>()
);

// @TODO create a store supporting both localStorage
export const addIntoSeen = createAction(
  '[Seen Component] AddIntoSeen',
  props<{ id: string }>()
);
export const addToSee = createAction(
  '[ToSee Component] AddToSee',
  props<{ id: string }>()
);
