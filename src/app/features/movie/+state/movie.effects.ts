import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { MovieService, response } from '../api/movie.service';
import { Movie } from '../movie.model';
import * as MovieActions from './movie.actions';

@Injectable()
export class MovieEffects {
  // Effect used by default, query service & return result on reducer by loadSuccess
  loadMovies$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MovieActions.loadDefault),
      mergeMap(() =>
        this.moviesService.getAll().pipe(
          map((movies) => {
            return MovieActions.loadSuccess({
              payload: movies.results as Movie[],
            });
          }),
          catchError(() => EMPTY)
        )
      )
    )
  );

  // Effect used by search, query service & return result on reducer by loadSuccess
  searchMovies$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MovieActions.loadBySearch),
      mergeMap((props) => {
        return this.moviesService.getByQuery(props.query, props.page).pipe(
          map((result) => {
            return MovieActions.loadSuccess({
              payload: result.results as Movie[],
            });
          }),
          catchError(() => EMPTY)
        );
      })
    )
  );

  // Effect used once user get detail of item, query service & return result on reducer by loadSuccess
  searchMediaById = createEffect(() =>
    this.actions$.pipe(
      ofType(MovieActions.getById),
      mergeMap((props) => {
        return this.moviesService.getById(props.mediaType, props._id).pipe(
          map((result) => {
            console.log(result);
            return MovieActions.getByIdSuccess({
              payload: result as Movie,
            });
          }),
          catchError(() => EMPTY)
        );
      })
    )
  );

  constructor(private actions$: Actions, private moviesService: MovieService) {}
}
