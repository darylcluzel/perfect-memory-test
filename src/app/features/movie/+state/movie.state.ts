import { Movie } from '../movie.model';

export default class MovieState {
  Movies: Array<Movie> | Movie = [];
  MovieError: any;
}

export const initializeState = (): MovieState => {
  return { Movies: Array<Movie>(), MovieError: null };
};
