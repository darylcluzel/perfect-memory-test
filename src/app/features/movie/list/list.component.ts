import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import MovieState from '../+state/movie.state';
import { Movie } from '../movie.model';
import * as MovieActions from '../+state/movie.actions';

import { UntilDestroy } from '@ngneat/until-destroy';
import { BottomSheetData } from 'src/app/shared/bottom-sheet/bottom-sheet.component';
import { ConfigService } from '../../../core/config.service';

@UntilDestroy()
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  public movies$: Observable<Movie[]>;
  public bottomSheetData: BottomSheetData[] = [];
  // public actualPage: number = 1;

  constructor(
    private store: Store<{ MovieFeature: { movies: MovieState } }>,
    private config: ConfigService
  ) {
    // Select movies stored
    this.movies$ = this.store.select((data: any) => {
      const state = data.MovieFeature.movies.Movies as Movie[];
      return state;
    });
    this.store.dispatch(MovieActions.loadDefault());
  }

  ngOnInit(): void {
    this.getBottomSheetConfig();
  }

  /**
   * Get json data to configure templater
   */
  async getBottomSheetConfig(): Promise<void> {
    this.bottomSheetData = await this.config.get('browse_bottom_sheet');
  }

  // @TODO Infinite scroll, require new actions & data manipulating on store
  // Find a way less dirty to get input value
  //
  // onScroll($event: any) {
  //   this.actualPage++;
  //   const query = (document.getElementById('search') as HTMLInputElement)
  //     ?.value;
  //   if (query) {
  //     this.store.dispatch(
  //       MovieActions.loadBySearch({ query, page: this.actualPage })
  //     );
  //   }
  // }
}
