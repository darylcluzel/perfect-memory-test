import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyMediaComponent } from './my-media.component';
import { SeenComponent } from './seen/seen.component';
import { ToSeeComponent } from './to-see/to-see.component';

const routes: Routes = [
  {
    path: '',
    component: MyMediaComponent,
    children: [
      {
        path: '',
        redirectTo: 'to-see',
        pathMatch: 'full',
      },
      {
        path: 'seen',
        component: SeenComponent,
      },
      {
        path: 'to-see',
        component: ToSeeComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

  exports: [RouterModule],
})
export class MyMediaRoutingModule {}
