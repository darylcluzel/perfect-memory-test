import { Component, OnInit } from '@angular/core';
import { NavRoutes } from 'src/app/core/shell/shell.component';
import { ConfigService } from '../../core/config.service';

@Component({
  selector: 'app-my-media',
  templateUrl: './my-media.component.html',
  styleUrls: ['./my-media.component.scss'],
})
export class MyMediaComponent implements OnInit {
  public navLinks: NavRoutes[] = [{}];

  constructor(private config: ConfigService) {}

  ngOnInit() {
    this.getLoadRoute();
  }

  /**
   * Get json data to configure tabs
   */
  async getLoadRoute(): Promise<void> {
    this.navLinks = await this.config.get('my_media_routes');
  }
}
