import { Component, OnInit } from '@angular/core';
import { Movie } from '../../movie/movie.model';
import { StorageFacade } from '../../storage/storage-facade';
import { ConfigService } from '../../../core/config.service';
import { BottomSheetData } from 'src/app/shared/bottom-sheet/bottom-sheet.component';

@Component({
  selector: 'app-seen',
  templateUrl: './seen.component.html',
  styleUrls: ['./seen.component.scss'],
})
export class SeenComponent implements OnInit {
  public medias: Movie[] = [];
  public bottomSheetData: BottomSheetData[] = [];

  constructor(private storage: StorageFacade, private config: ConfigService) {}

  ngOnInit() {
    this.medias = this.storage.getMedia('seen');
    this.getBottomSheetConfig();
  }

  /**
   * Get json data to configure templater
   */
  async getBottomSheetConfig(): Promise<void> {
    this.bottomSheetData = await this.config.get('seen_bottom_sheet');
  }
}
