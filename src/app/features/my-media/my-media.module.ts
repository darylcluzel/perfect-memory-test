import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyMediaComponent } from './my-media.component';
import { ToSeeComponent } from './to-see/to-see.component';
import { SeenComponent } from './seen/seen.component';
import { MyMediaRoutingModule } from './my-media-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { MatTabsModule } from '@angular/material/tabs';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MyMediaRoutingModule,
    MatTabsModule,
    MatBottomSheetModule,
  ],
  declarations: [MyMediaComponent, ToSeeComponent, SeenComponent],
})
export class MyMediaModule {}
