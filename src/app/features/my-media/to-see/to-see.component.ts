import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/core/config.service';
import { BottomSheetData } from 'src/app/shared/bottom-sheet/bottom-sheet.component';
import { Movie } from '../../movie/movie.model';
import { StorageFacade } from '../../storage/storage-facade';

@Component({
  selector: 'app-to-see',
  templateUrl: './to-see.component.html',
  styleUrls: ['./to-see.component.scss'],
})
export class ToSeeComponent implements OnInit {
  public medias: Movie[] = [];
  public data = [];
  public bottomSheetData: BottomSheetData[] = [];
  constructor(private storage: StorageFacade, private config: ConfigService) {}

  ngOnInit(): void {
    this.medias = this.storage.getMedia('to-see');
    // this.storage
    //   .getDataSubscriber()
    //   .subscribe((value) => (this.medias = value));
    this.getBottomSheetConfig();
  }

  /**
   * Get json data to configure templater
   */
  async getBottomSheetConfig(): Promise<void> {
    this.bottomSheetData = await this.config.get('to_see_bottom_sheet');
  }
}
