import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { Shell } from './core/shell/shell.service';

const routes: Routes = [
  Shell.childRoutes([
    { path: '', redirectTo: '/media', pathMatch: 'full' },
    {
      path: 'media',
      loadChildren: () =>
        import('./features/movie/movie.module').then((m) => m.MovieModule),
    },
    {
      path: 'my-media',
      loadChildren: () =>
        import('./features/my-media/my-media.module').then(
          (m) => m.MyMediaModule
        ),
    },
  ]),
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
