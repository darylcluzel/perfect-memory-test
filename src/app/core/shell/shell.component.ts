import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { ConfigService } from '../config.service';

export interface NavRoutes {
  path?: string;
  icon?: string;
  title?: string;
}

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
})
export class ShellComponent implements OnInit {
  @ViewChild('sidenav') sidenav?: MatSidenav;

  public browseRoutes: NavRoutes[] = [{}];
  public myMediaRoutes: NavRoutes[] = [{}];

  constructor(private config: ConfigService) {}

  ngOnInit(): void {
    this.getLoadRoute();
  }

  /**
   * Get json data to configure menu items
   */
  async getLoadRoute(): Promise<void> {
    this.browseRoutes = await this.config.get('browse_routes');
    this.myMediaRoutes = await this.config.get('my_media_routes');
  }

  toggleSideNav(): void {
    this.sidenav?.open();
  }
}
