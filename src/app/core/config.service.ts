import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  private uriPrefix = 'assets/config/';
  private uriSuffix = '.json';

  constructor(private httpClient: HttpClient) {}

  /**
   * Simply get a config file
   * @param filename the name of the file
   * @returns
   */
  public get(filename: string): Promise<any> {
    return this.httpClient
      .get<any>(this.uriPrefix + filename + this.uriSuffix)
      .toPromise();
  }
}
