import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedComponent } from './shared.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { SnackBarComponent } from './snack-bar/snack-bar.component';
import { BottomSheetComponent } from './bottom-sheet/bottom-sheet.component';
import { MatListModule } from '@angular/material/list';
import { ListMediaTemplateComponent } from './list-media-template/list-media-template.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  imports: [
    CommonModule,
    MatSnackBarModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
  ],
  declarations: [
    SharedComponent,
    SnackBarComponent,
    BottomSheetComponent,
    ListMediaTemplateComponent,
  ],
  exports: [ListMediaTemplateComponent],
})
export class SharedModule {}
