import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Movie } from 'src/app/features/movie/movie.model';
import { MediaType } from '../../features/movie/movie.model';
import {
  BottomSheetComponent,
  BottomSheetData,
} from '../bottom-sheet/bottom-sheet.component';
import { SnackBarComponent } from '../snack-bar/snack-bar.component';
import { StorageFacade } from '../../features/storage/storage-facade';

@Component({
  selector: 'app-list-media-template',
  templateUrl: './list-media-template.component.html',
  styleUrls: ['./list-media-template.component.scss'],
})
export class ListMediaTemplateComponent implements OnInit {
  @Input() medias: Movie[] | null = null;
  @Input() type: string = '';
  @Input() bottomSheetData: BottomSheetData[] = [];
  @Input() component: any;
  constructor(
    private _bottomSheet: MatBottomSheet,
    private _snackBar: MatSnackBar,
    private storage: StorageFacade,
    private router: Router
  ) {}

  ngOnInit() {}

  /**
   * Use router to navigate to details
   * @param $event the selected media
   * @returns
   */
  onListItemClick($event: any): void {
    if (!$event.id) {
      return;
    }
    this.router.navigate(['media', $event.id], { fragment: $event.type });
  }

  /**
   * Open Bottom Sheet in generic way.
   * Once bottom sheet closed trigger the snack bar in generic way.
   * Act on localStorage thanks to the json config file to know what to do.
   * @TODO: Dirty method : refresh url instead of subscribe localStorage data
   * @param media user interested for
   */
  openBottomSheet(media: Movie): void {
    this._bottomSheet.open(BottomSheetComponent, {
      data: this.bottomSheetData,
    });
    this._bottomSheet._openedBottomSheetRef
      ?.afterDismissed()
      .subscribe((value: { func: keyof StorageFacade; type: string }) => {
        if (value) {
          this.router
            .navigateByUrl('/', { skipLocationChange: true })
            .then(() => {
              this.router.navigate([this.component]);
            });
          this.openSnackBar({
            options: { value, mediaTitle: media.title || media.name },
            success: this.storage[value.func](
              media as Movie & string,
              value.type
            ) as boolean | string,
          });
        }
      });
  }

  /**
   * Open the snack bar & configure it
   * @param data data contains the json config file plus the result of the localStorage action
   */
  openSnackBar(data: { options: any; success: boolean | string }): void {
    const cssClass =
      typeof data.success === 'string' || !data.success
        ? 'snack-bar-error'
        : 'snack-bar-valid';
    this._snackBar.openFromComponent(SnackBarComponent, {
      duration: 5000,
      data,
      panelClass: cssClass,
    });
  }
}
