import { Component, Inject, InjectionToken, OnInit } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { VirtualTimeScheduler } from 'rxjs';

const ADD_MEDIA = 'addMedia';
const REMOVE_MEDIA = 'removeMedia';
const CHANGE_MEDIA_LIST = 'changeList';
const _LIST = ' list ';

@Component({
  selector: 'app-snack-bar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.scss'],
})
export class SnackBarComponent implements OnInit {
  public text = '';

  // Inject data from bottom sheet with the status of the localStorage response
  constructor(
    @Inject(MAT_SNACK_BAR_DATA)
    private payload: {
      options: { value: { func: string; type: string }; mediaTitle: string };
      success: boolean | string;
    }
  ) {}

  ngOnInit(): void {
    this.text = this.formatData();
  }

  /**
   * Format data from json config file to readable text.
   * @TODO: Create an Error Flow to cancel this dirty switch
   * @returns formated text
   */
  formatData(): string {
    console.log(this.payload);
    let text: string = this.payload.options.mediaTitle;
    const type = this.payload.options.value.type.replace('-', ' ');
    if (typeof this.payload.success === 'boolean') {
      switch (this.payload.options.value.func) {
        case ADD_MEDIA:
          if (this.payload.success) {
            text += ' has been added to your ' + type + _LIST;
          } else {
            text += ' already in your ' + type + _LIST;
          }
          break;
        case REMOVE_MEDIA:
          text += ' has been removed from your ' + type + _LIST;
          break;
        case CHANGE_MEDIA_LIST:
          text += ' has been moved to your ';
          text += type === 'seen' ? 'to see' : 'seen';
          text += _LIST;
      }
    } else {
      text += this.payload.success + _LIST;
    }
    return text;
  }
}
